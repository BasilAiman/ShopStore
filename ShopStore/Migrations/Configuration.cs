namespace ShopStore.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using ShopStore.Entites;
    using ShopStore.Entites.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ShopStore.Entites.StoreContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ShopStore.Entites.StoreContext context)
        {

            var UserManager = new ShopUserManager(new ShopUserStore(context));
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            string username = "Admin";
            string password = "123456"; //Change for more complciated later

            if (!RoleManager.RoleExists(username))
            {
                var roleresult = RoleManager.Create(new IdentityRole(username));

            }

            var applicationUser = new ApplicationUser()
            {
                UserName = username
            };

            var adminresult = UserManager.Create(applicationUser, password);
            if (adminresult.Succeeded)
            {

                var result = UserManager.AddToRole(applicationUser.Id, username);

            }

            //Adding Data
            //Dan Brown BOoks
            List<Book> Dan_Books = new List<Book>();
            Dan_Books.Add(new Book()
            {
                Name = "The Davinci Code",
                Description = "An ingenious code hidden in the works of Leonardo da Vinci. A desperate race through the cathedrals and castles of Europe. An astonishing truth concealed for centuries . . . unveiled at last.",
                numberOfPages = 481,
                ReleaseDate = new DateTime(2016, 3, 28)
            });
            Dan_Books.Add(new Book()
            {
                Name = "Deception Point",
                Description = "When a new NASA satellite spots evidence of an astonishingly rare object buried deep in the Arctic ice, the floundering space agency proclaims a much-needed victory...a victory that has profound implications for U.S. space policy and the impending presidential election. ",
                numberOfPages = 556,
                ReleaseDate = new DateTime(2002, 11, 26)
            });
            Dan_Books.Add(new Book()
            {
                Name = "The Lost Symbol",
                Description = "In this stunning follow-up to the global phenomenon The Da Vinci Code, Dan Brown demonstrates once again why he is the world's most popular thriller writer. The Lost Symbol is a masterstroke of storytelling - a deadly race through a real-world labyrinth of codes, secrets, and unseen truths",
                numberOfPages = 509,
                ReleaseDate = new DateTime(2009, 9, 15)
            });
            context.Author.Add(new Author()
            {
                Name = "Dan Brown",
                Description = "Daniel Gerhard \"Dan\" Brown (born June 22, 1964) is an American author of thriller fiction who is best known for the 2003 bestselling novel The Da Vinci Code.",
                Books= Dan_Books
            });
            //------------------------------------
            //John R R Tolkien
            List<Book> John_Books = new List<Book>();
            John_Books.Add(new Book()
            {
                Name = "The Hobbit",
                Description = "In a hole in the ground there lived a hobbit. Not a nasty, dirty, wet hole, filled with the ends of worms and an oozy smell, nor yet a dry, bare, sandy hole with nothing in it to sit down on or to eat: it was a hobbit-hole, and that means comfort.",
                numberOfPages = 366,
                ReleaseDate = new DateTime(2002, 8, 15)
            });
            John_Books.Add(new Book()
            {
                Name = "The Lord of the Rings",
                Description = "A fantastic starter set for new Tolkien fans or readers interested in rediscovering the magic of Middle-earth, this three-volume box set features paperback editions of the complete trilogy -- The Fellowship of the Ring, The Two Towers, and The Return of the King -- each with art from the New Line Productions feature film on the cover.",
                numberOfPages = 1216,
                ReleaseDate = new DateTime(2005, 10, 12)
            });
            John_Books.Add(new Book()
            {
                Name = "The Silmarillion",
                Description = "The story of the creation of the world and of the First Age, this is the ancient drama to which the characters in The Lord of the Rings look back and in whose events some of them, such as Elrond and Galadriel, took part. ",
                numberOfPages = 386,
                ReleaseDate = new DateTime(2004, 11, 15)
            });
            context.Author.Add(new Author()
            {
                Name = "J. R. R. Tolkien",
                Description = "John Ronald Reuel Tolkien, was an English writer, poet, philologist, and university professor who is  known as the author of high-fantasy works The Hobbit, The Lord of the Rings, and The Silmarillion.",
                Books = John_Books
            });
            //-------------------------------------
            List<Book> George_Books = new List<Book>();
            George_Books.Add(new Book()
            {
                Name = "A Game Of Thrones",
                Description = "As Warden of the north, Lord Eddard Stark counts it a curse when King Robert bestows on him the office of the Hand. His honour weighs him down at court where a true man does what he will, not what he must � and a dead enemy is a thing of beauty.",
                numberOfPages = 835,
                ReleaseDate = new DateTime(1996, 8, 6)
            });
            George_Books.Add(new Book()
            {
                Name = "A Clash of Kings",
                Description = "Time is out of joint. The summer of peace and plenty, ten years long, is drawing to a close, and the harsh, chill winter approaches like an angry beast. Two great leaders�Lord Eddard Stark and Robert Baratheon�who held sway over an age of enforced peace are dead...victims of royal treachery.",
                numberOfPages = 761,
                ReleaseDate = new DateTime(1998, 11, 16)
            });
            George_Books.Add(new Book()
            {
                Name = "A Storm of Swords ",
                Description = "nd A Clash of Kings. Together, this series comprises a genuine masterpiece of modern fantasy, destined to stand as one of the great achievements of imaginative fiction.",
                numberOfPages = 1177,
                ReleaseDate = new DateTime(2000, 8, 8),
            });
            context.Author.Add(new Author()
            {
                Name = "George R. R. Martin",
                Description = "George Raymond Richard Martin, often referred to as GRRM, is an American novelist and short-story writer in the fantasy, horror, and science fiction genres, screenwriter, and television producer.",
                Books = George_Books
            });

            //---------------------------------
            List<Book> Rowling_Books = new List<Book>();
            Rowling_Books.Add(new Book()
            {
                Name = "Harry Potter and the Sorcerer's Stone ",
                Description = "Harry Potter's life is miserable. His parents are dead and he's stuck with his heartless relatives, who force him to live in a tiny closet under the stairs. But his fortune changes when he receives a letter that tells him the truth about himself",
                numberOfPages = 320,
                ReleaseDate = new DateTime(1997,7, 26)
            });
            Rowling_Books.Add(new Book()
            {
                Name = "Harry Potter and the Prisoner of Azkaban",
                Description = "Harry Potter is lucky to reach the age of thirteen, since he has already survived the murderous attacks of the feared Dark Lord on more than one occasion. But his hopes for a quiet term concentrating on Quidditch are dashed when a maniacal mass-murderer escapes from Azkaban, pursued by the soul-sucking Dementors who guard the prison.",
                numberOfPages = 435,
                ReleaseDate = new DateTime(1999, 7, 8)
            });
            Rowling_Books.Add(new Book()
            {
                Name = "Harry Potter and the Goblet of Fire",
                Description = "Harry Potter is midway through both his training as a wizard and his coming of age. Harry wants to get away from the pernicious Dursleys and go to the International Quidditch Cup with Hermione, Ron, and the Weasleys.",
                numberOfPages = 734,
                ReleaseDate = new DateTime(2000, 8, 8),
            });
            context.Author.Add(new Author()
            {
                Name = "J. K. Rowling",
                Description = "Joanne \"Jo\" Rowling, OBE, FRSL, pen names J. K. Rowling and Robert Galbraith, is a British novelist, screenwriter and film producer best known as the author of the Harry Potter fantasy series",
                Books = Rowling_Books
            });





        }
    }
}
