(function($){
  $(function(){
      $('.modal').modal();
      $('select').material_select();

    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $("#signupForm").validate({
        rules: {
            username: {
                required: true,
                minlength: 5
            },
            Name: {
                required: true,
                minlength: 5
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            },
            cpassword: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            address: {
                required: true
            },
            MobilePhone: {
                required: true,
                minlength: 11
            },
            cgender: "required",
        },
        //For custom messages
        messages: {
            username: {
                required: "Enter a username",
                minlength: "Enter at least 5 characters"
            },
            curl: "Enter your website",
        },
        errorElement: 'div',
        errorPlacement: function (error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });

  }); // end of document ready
})(jQuery); // end of jQuery name space