﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ShopStore.Entites;
using ShopStore.Entites.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ShopStore.Core
{
    public class DatabaseInit : DropCreateDatabaseAlways<StoreContext>
    {
        protected override void Seed(StoreContext context)
        {
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>());
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());

            string username = "Admin";
            string password = "123456"; //Change for more complciated later

            if (!RoleManager.RoleExists(username))
            {
                var roleresult = RoleManager.Create(new IdentityRole(username));

            }

            var applicationUser = new ApplicationUser()
            {
                UserName = username
            };
           
            var adminresult = UserManager.Create(applicationUser, password);
            if (adminresult.Succeeded)
            {

              var result = UserManager.AddToRole(applicationUser.Id, username);

            }
            base.Seed(context);


        }
    }

}