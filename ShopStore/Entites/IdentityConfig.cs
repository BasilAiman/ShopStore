﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ShopStore.Entites.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopStore.Entites
{
    public class ShopUserStore: UserStore<ApplicationUser>
    {
        public ShopUserStore():this(new StoreContext() { })
        {

        }
        public ShopUserStore(StoreContext context):base(context)
        {

        }
    }
    public class ShopUserManager : UserManager<ApplicationUser>
    {
        public ShopUserManager()
            :this(new ShopUserStore())
        {

        }
        public ShopUserManager(UserStore<ApplicationUser> userStore):base(userStore)
        {

        }
    }
}