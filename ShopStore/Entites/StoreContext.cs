﻿using Microsoft.AspNet.Identity.EntityFramework;
using ShopStore.Entites.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ShopStore.Entites
{
    public class StoreContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Author { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public StoreContext():base("StoreConnection")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
           
        }

    }
}