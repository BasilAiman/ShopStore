﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopStore.Entites.Models
{
    public class Book
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Author")]
        public int AuthorId { get; set; }
        public virtual Author Author { get; set; }
        public string Name { get; set; }
        public DateTime ReleaseDate { get; set; }
        public int numberOfPages { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Orders> Orders { get; set; }

    }
}