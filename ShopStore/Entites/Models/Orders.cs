﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShopStore.Entites.Models
{
    public class Orders
    {
        [Key]
        public int id { get; set; }
        public string Notes { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
        [ForeignKey("Book")]
        public int bookId { get; set; }
        public virtual Book Book { get; set; }
        public DateTime Date { get; set; }
    }
}