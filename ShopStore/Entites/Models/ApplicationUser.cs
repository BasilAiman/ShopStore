﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopStore.Entites.Models
{
    public class ApplicationUser : IdentityUser
    {

        public string FullName { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string MobilePhone { get; set; }
        public virtual ICollection<Orders> Orders { get; set; }

    }
}