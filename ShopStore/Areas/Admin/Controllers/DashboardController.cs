﻿using ShopStore.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopStore.Areas.Admin.Controllers
{
    public class DashboardController : Controller
    {

        //
        // GET: /Admin/Dashboard/
        [Authorize(Roles ="Admin")]
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Orders()
        {
            StoreContext context = new StoreContext();
            return View(context.Orders);
        }
	}
}