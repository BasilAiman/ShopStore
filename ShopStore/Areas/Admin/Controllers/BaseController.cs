﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopStore.Areas.Admin.Controllers
{
    public class BaseController : Controller
    {
        public void saveImage(HttpPostedFileBase img, int id, string folderName)
        {
            if (img != null)
            {
                string path = string.Format("/img/{0}/{1}.jpg", folderName, id);
                var machinePath = Server.MapPath(path);
                if (System.IO.File.Exists(machinePath))
                    System.IO.File.Delete(machinePath);
                img.SaveAs(machinePath);
            }

        }
        public void saveImage(HttpPostedFileBase img, int id, string folderName, string ext)
        {
            if (img != null)
            {
                string path = string.Format("/img/{0}/{1}.{2}", folderName, id, ext);
                var machinePath = Server.MapPath(path);
                if (System.IO.File.Exists(machinePath))
                    System.IO.File.Delete(machinePath);
                img.SaveAs(machinePath);
            }

        }
        public void saveImage(HttpPostedFileBase img, string name, string folderName)
        {
            if (img != null)
            {
                string path = string.Format("/img/{0}/{1}.jpg", folderName, name);
                var machinePath = Server.MapPath(path);
                if (System.IO.File.Exists(machinePath))
                    System.IO.File.Delete(machinePath);
                img.SaveAs(machinePath);
            }

        }
        public void saveImage(HttpPostedFileBase img, string name, string folderName, string ext)
        {
            if (img != null)
            {
                string path = string.Format("/img/{0}/{1}.{2}", folderName, name, ext);
                var machinePath = Server.MapPath(path);
                if (System.IO.File.Exists(machinePath))
                    System.IO.File.Delete(machinePath);
                img.SaveAs(machinePath);
            }

        }
        public void SaveFile(HttpPostedFileBase file, string folderName, string fileName)
        {
            if (file != null)
            {
                string path = string.Format("/{0}/{1}", folderName, fileName);
                var machinePath = Server.MapPath(path);
                if (System.IO.File.Exists(machinePath))
                    System.IO.File.Delete(machinePath);
                file.SaveAs(machinePath);
            }
        }
        public void DeleteFile(string folderName, string fileName)
        {
            string path = string.Format("/{0}/{1}", folderName, fileName);
            var machinePath = Server.MapPath(path);
            if (System.IO.File.Exists(machinePath))
                System.IO.File.Delete(machinePath);
        }

        public void deleteImage(int id, string folderName)
        {
            string path = string.Format("/img/{0}/{1}.jpg", folderName, id);
            var machinePath = Server.MapPath(path);
            if (System.IO.File.Exists(machinePath))
                System.IO.File.Delete(machinePath);
        }
        public void deleteImage(string name, string folderName)
        {
            string path = string.Format("/img/{0}/{1}.jpg", folderName, name);
            var machinePath = Server.MapPath(path);
            if (System.IO.File.Exists(machinePath))
                System.IO.File.Delete(machinePath);
        }
    }
}