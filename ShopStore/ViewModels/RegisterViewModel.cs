﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopStore.ViewModels
{
    public class RegisterViewModel
    {
        public string Name { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string address { get; set; }
        public string MobilePhone { get; set; }
        public string cgender { get; set; }

    }
}