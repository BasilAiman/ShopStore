﻿using ShopStore.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ShopStore.Controllers
{
    public class OrdersController : Controller
    {
        ShopUserManager manager = new ShopUserManager();


        // GET: Orders
        [Authorize]
        public async Task<ActionResult> Index()
        {
            var username = User.Identity.Name;
            var user = await manager.FindByNameAsync(username);
            StoreContext context = new StoreContext();
            var res = context.Orders.Where(x => x.UserId == user.Id);
            return View(res);
        }
    }
}