﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ShopStore.Core;
using ShopStore.Entites;
using ShopStore.Entites.Models;
using ShopStore.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopStore.Controllers
{
    public class HomeController : Controller
    {
       

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Register()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Books");

            return View();
        }
        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            using (var _manager = new ShopUserManager())
            {
                ApplicationUser user = new ApplicationUser();
                user.UserName = model.username;
                user.Gender = model.cgender;
                user.Email = model.email;
                user.Address = model.address;
                user.FullName = model.Name;
                user.MobilePhone = model.MobilePhone;
                user.PhoneNumberConfirmed = true;
                user.EmailConfirmed = true;

                var result = _manager.Create(user, model.password);
                if (result.Succeeded)
                {
                    var Identity =   _manager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                    var authManager = HttpContext.GetOwinContext().Authentication;
                    authManager.SignIn(
                        new AuthenticationProperties() { IsPersistent = false },
                        Identity);
                    
                    return RedirectToAction("Index", "Books");
                }
                else
                    return View(result.Errors);
            }
          
        }

        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            using (var _manager = new ShopUserManager())
            {
                var user = _manager.Find(model.UserName,model.Password);
                if (user != null)
                {
                    var identity = _manager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                    var authManager = HttpContext.GetOwinContext().Authentication;
                    authManager.SignIn(
                        new AuthenticationProperties() { IsPersistent = false },
                        identity);
                    
                    if(model.UserName.ToLower() == "admin")
                        return RedirectToAction("Index", "Dashboard", new { area = "Admin" });
                    return RedirectToAction("Index", "Books");
                }
                else {
                    List<string> err = new List<string>();
                    err.Add("The username or password are invalid");
                    return View(err);
                }
            }
            

        }

        

        public ActionResult Logout()
        {
            var auth = HttpContext.GetOwinContext().Authentication;
            auth.SignOut();
            return RedirectToAction("Index");
            
        }

        [Authorize]
        public ActionResult Auth()
        {
            return View();
        }
    }
}