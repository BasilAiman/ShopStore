﻿using ShopStore.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShopStore.Controllers
{
    public class AuthorsController : Controller
    {
        StoreContext context = new StoreContext();

        // GET: Authors
        public ActionResult Index()
        {
            return View(context.Author.AsEnumerable());
        }
        public ActionResult Books(int id)
        {
            var res = context.Books.Where(x => x.AuthorId == id);
            return View(res);
        }
    }

}