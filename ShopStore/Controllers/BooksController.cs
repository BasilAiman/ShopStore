﻿using ShopStore.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ShopStore.Controllers
{
    public class BooksController : Controller
    {
        ShopUserManager manager = new ShopUserManager();
        StoreContext context = new StoreContext();
        // GET: Books
        public ActionResult Index(string query = "", int AuthorId = 0)
        {
            ViewBag.Authors = context.Author.AsQueryable();

            var books = context.Books.AsQueryable();

            if (AuthorId != 0) {
                books = books.Where(x => x.AuthorId == AuthorId);
            }
            if (query != "") {
                books = books.Where(x => x.Name.Contains(query) || x.Description.Contains(query));
            }

            return View(books);
        }
        [Authorize]
        public async Task<HttpStatusCodeResult> Order(int id)
        {
            string username = User.Identity.Name;
            var user = await manager.FindByNameAsync(username);
            if (user == null)
            {
                return new HttpStatusCodeResult(500);
            }
            context.Orders.Add(new Entites.Models.Orders() {
                UserId = user.Id,
                bookId = id,
                Date = DateTime.Now
            });
            context.SaveChanges();
            return new HttpStatusCodeResult(HttpStatusCode.OK);

        }
    }
}